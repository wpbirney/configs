set fish_greeting

set PATH '/home/blackbeard/bin' $PATH
set -x EDITOR vim

set htpc 'wpbirney@192.168.1.2:Desktop'

#set up ls colors
eval (dircolors -c ~/.dircolors | sed 's/>&\/dev\/null$//')

#function ls
#	command ls --color=auto --group-directories-first $argv
#end

function rsync
	command rsync --progress $argv
end

function mosh
	set -x LANG en_US.UTF-8
	command mosh --server='mosh-server new -l LANG=en_US.UTF-8' $argv
end

function df 
	btrfs filesystem df /
end

function lss
	ls *.cpp *.h Makefile *.cbp
end

function nocaps
	setxkbmap -option ctrl:nocaps
end

function caps
	setxkbmap -option
end

function gpa
	git push; git push rpi
end
